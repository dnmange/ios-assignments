//Question 1
func squaredSums(_ input: [Int]) -> Int {
    var sum: Int = 0;
    input.forEach { value in
        sum += value*value
    }
    return sum
}
squaredSums([1,2,3])

//Question 2
func squaredSums2(_ input: [Int]) -> Int {
    var sum: Int = 0
    input.forEach { value in
        if value%2 == 0 {
            sum += value*value
        }
    }
    return sum
}
squaredSums2([1,2,3,5,4])

//Question 3
func squaredSums3(_ input: [Int?]) -> Int {
    var sum: Int = 0;
    input.forEach { value in
        if let hasValue = value {
            if hasValue%2 == 0 {
                sum += hasValue*hasValue
            }
        }
    }
    return sum
}
var optionalInput: [Int?] = [1,2,3,nil]
squaredSums3(optionalInput)
squaredSums3([1,2,3,4])
squaredSums3([nil])

//Question 4
func squaredSums4(_ input: [Int]?) -> Int? {
    if let hasInput = input {
        var sum = 0
        hasInput.forEach { value in
            if value%2 == 0 {
                sum += value*value
            }
        }
        return sum
    }
    return nil
}
squaredSums4(nil);
squaredSums4([1,2,3])

//Question 5
func squaredSums5(_ input: [Int]) -> Int {
    let sum: Int
    func isEven(value: Int) -> Bool {
        return value%2==0
    }
    sum = input.filter(isEven).reduce(0, { currentSum,value in
        currentSum + value*value
    })
    return sum;
}
squaredSums5([1,2,3,4])

//Question 6
func squaredSums6<T: Collection>(_ input: T) -> Int where T.Element == Int {
    var sum: Int = 0;
    input.forEach { value in
        if value%2 == 0 {
            sum += value*value
        }
    }
    return sum;
}

func squaredSums6<T:Collection>(_ input: T) -> Int where T.Element == (key: Int,value: Int) {
    var sum: Int = 0;
    input.forEach { key,value in
        if value%2 == 0 {
            sum += value*value
        }
    }
    return sum;
}

//check array
squaredSums6([1,2]);

//check dictionary
squaredSums6([1:2,2:3])

//check sets
let setInput: Set = [1,2,3,4,7]
squaredSums6(setInput)


//Question 7
struct Student{
    let redID: Int;
    let name: String;
    
    var numberOfUnits: Int;
    var gpa: Double;
    
    func priority() -> Double {
        return Double(numberOfUnits)*gpa
    }
}

var student: Student = Student(redID: 8363743, name: "Darshan", numberOfUnits: 120, gpa: 3.3)
print("Printing student priority: \(student.priority())\n")

//Question 8
class PriorityQueue {
    
    private var queue: [Student]
    var count: Int {
        get {
            return queue.count
        }
    }
    
    init() {
        queue = Array()
    }
    
    /*
     * Student is added at end of the queue,
     * this function utilize bottom up approach to find max student priority
     * and place it to the root of queue. maxHeap approach is used here.
     * */
    private func heapifyAdd(_ n: Int) {
        if (n < 0){
           return;
        }
    
        let left: Int = 2*n + 1
        let right: Int = 2*n + 2;
        
        if (left < queue.count && queue[n].priority() < queue[left].priority()){
            swap(left,n);
            heapifyAdd((n-1)/2);
        }else if (right < queue.count && queue[n].priority() < queue[right].priority()){
            swap(right,n);
            heapifyAdd((n-1)/2);
        }
    }
    /*
     * Student at root is removed and placed at end of the queue,
     * this function utilizes top down approach to find next max priority student and place it root
     * and this method is also based on creating maxHeap.
     * */
    private func heapifyRemove(_ n: Int){
        if(n>=queue.count) {
            return;
        }
    
        let left: Int = 2*n+1;
        let right: Int = 2*n+2;
    
        var largest: Int = n;
    
        if(left < queue.count && queue[largest].priority() < queue[left].priority()){
            largest = left;
        }
    
        if(right < queue.count && queue[largest].priority() < queue[right].priority()){
            largest = right;
        }
    
        if (n != largest){
            swap(largest,n);
            heapifyRemove(largest);
        }
    }
    
    private func swap(_ a:Int, _ b:Int) {
        let element: Student = queue[a]
        queue[a] = queue[b]
        queue[b] = element
    }
    
    public func add(_ student: Student) {
        queue.append(student)
        heapifyAdd(queue.count/2-1)
    }
    
    /*
     * Removes a student having highest priority
    */
    public func removeFirst() -> Student {
        swap(0,queue.count-1);
        let element: Student = queue.remove(at: queue.count-1);
        heapifyRemove(0);
        return element;
    }
    
    /*
     * Prints sorted student information according to higher priority
    */
    public func display() -> [Student]{
    
        let studentsCopy: [Student] = Array(queue);
        var result: [Student] = Array();
        var i: Int = queue.count;
        while(i != 0){
            let student: Student = self.removeFirst();
            result.append(student);
            print("Name: \(student.name) RedId: \(student.redID) Priority: \(student.priority()) \n");
            i -= 1;
        }
    
        queue = studentsCopy;
        return result;
    }
    
    public func first() -> Student {
        return queue[0];
    }
    
    public func enumerated() -> EnumeratedSequence<Array<Student>>{
        return queue.enumerated()
    }
}

//Test priority queue
var student1: Student = Student(redID: 8222019, name: "Darshan", numberOfUnits: 50, gpa: 3.2)
student1.priority()

var student2: Student = Student(redID: 8222029, name: "Nakul", numberOfUnits: 150, gpa: 3.5)
student2.priority()

var student3: Student = Student(redID: 8222015, name: "Vinod", numberOfUnits: 20, gpa: 3)
student3.priority()

var student4: Student = Student(redID: 8222014, name: "Ashish", numberOfUnits: 150, gpa: 4)
student4.priority()

var student5: Student = Student(redID: 8222020, name: "Aman", numberOfUnits: 100, gpa: 3.3)
student5.priority()

var queue: PriorityQueue = PriorityQueue()

queue.add(student1)

print("-------------------Printing Student Information according to higher priority--------------------------\n")
queue.display()


queue.add(student2)

print("-------------------Printing Student Information according to higher priority--------------------------\n")
queue.display()

queue.add(student3)

print("-------------------Printing Student Information according to higher priority--------------------------\n")
queue.display()

queue.add(student4)

print("-------------------Printing Student Information according to higher priority--------------------------\n")
queue.display()

queue.removeFirst()

print("-------------------Printing Student Information according to higher priority--------------------------\n")
queue.display()

queue.add(student5)

print("-------------------Printing Student Information according to higher priority--------------------------\n")
queue.display()

queue.removeFirst()

print("-------------------Printing Student Information according to higher priority--------------------------\n")
queue.display()

print("\nPeek:\(queue.first()), Size of Queue: \(queue.count)\n")
