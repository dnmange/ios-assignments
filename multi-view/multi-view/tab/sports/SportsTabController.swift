//
//  SportsTabController.swift
//  multi-view
//
//  Created by Darshan Mange on 10/27/18.
//  Copyright © 2018 Darshan Mange. All rights reserved.
//

import UIKit

class SportsTabController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var countrySports:Dictionary<String,Array<String>>?
    var selectedCountry:String?
    var selectedSport:String?
    var countries:Array<String>?    // stores sorted list of countries
    var sports:Array<String>?       // stores sorted list of sports
    
    @IBOutlet weak var countrySportsPicker: UIPickerView!
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        guard (countries != nil) && sports != nil else { return 0 }
        switch component {
            
            case 0: return countries!.count
            case 1: return sports!.count
            default: return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int,
                    forComponent component: Int) -> String? {
        
        guard (countries != nil) && sports != nil else { return "None" }
        switch component {
            
            case 0: return countries![row]
            case 1: return sports![row]
            default: return "None"
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int,
                    inComponent component: Int) {
        
        guard (countries != nil) && sports != nil else { return }
        if component == 0 {
            
            selectedCountry = countries![row]
            sports = countrySports![selectedCountry!]!.sorted()
            pickerView.reloadComponent(1)
        } else {
            
            selectedSport = sports![row]
            showAlert()
        }
        print("picked component \(component) row \(row)")
    }
    
    func showAlert() {
        
        var message: String = "\(selectedSport!) is the sport of \(selectedCountry!)"
        let alertController = UIAlertController(title: "Chosen Sports",
                                           message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(ok)
        self.present(alertController, animated: true, completion: {print("Done")})
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let data:Bundle = Bundle.main
        let foodPlist:String? = data.path(forResource: "sports", ofType: "plist")
        
        if foodPlist != nil {
            countrySports = (NSDictionary.init(contentsOfFile: foodPlist!) as! Dictionary)
            countries = countrySports?.keys.sorted()
            selectedCountry = countries![0]
            sports = countrySports![selectedCountry!]!.sorted()
        }
        
        if countrySportsPicker != nil {
            countrySportsPicker!.delegate = self
            countrySportsPicker!.dataSource = self
        }
    }
    

}
