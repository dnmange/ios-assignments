//
//  NameTabController.swift
//  multi-view
//
//  Created by Darshan Mange on 10/28/18.
//  Copyright © 2018 Darshan Mange. All rights reserved.
//

import UIKit

class NameTabController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var nameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tabController = tabBarController as! TabController
        
        // getting name from Main scene to this scene via TabController
        nameTextField.text = tabController.name
        nameTextField.returnKeyType = .done
        nameTextField.delegate = self
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        // dismissing the keyboard
        self.view.endEditing(true)
        return true
    }
}
