//
//  MapTabController.swift
//  multi-view
//
//  Created by Darshan Mange on 10/27/18.
//  Copyright © 2018 Darshan Mange. All rights reserved.
//

import UIKit
import MapKit

class MapTabController: UIViewController, MKMapViewDelegate, UITextFieldDelegate {

    var locationManager:CLLocationManager = CLLocationManager()
    
    var sourceCoordinates:CLLocationCoordinate2D?
    var destinationCoordinates:CLLocationCoordinate2D?
    
    var sourceAddress:String = ""
    var destinationAddress:String = ""

    @IBOutlet weak var destinationTextField: UITextField!
    @IBOutlet weak var sourceTextField: UITextField!
    @IBOutlet weak var mapView: MKMapView!
    
    @IBAction func getDirections(_ sender: Any) {
        
        if sourceTextField!.text == "" || destinationTextField!.text == "" {
            showAlert()
            return
        }
        
        // clearing the content on the maps
        mapView.removeAnnotations(mapView.annotations)
        mapView.removeOverlays(mapView.overlays)

        sourceAddress = sourceTextField.text!
        destinationAddress = destinationTextField.text!
        
        getCoordinates(source:sourceAddress)
    }
    
    /*
     * series of callbacks is executed to draw directions on map
     * when this method is called due to asynchronous nature of
     * geocodeAddressString method
     */
    func getCoordinates(source: String) {
        
        let locator = CLGeocoder()
        
        locator.geocodeAddressString(source)
        { (placemarks, errors) in
            
            if let place = placemarks?[0] {
                
                self.sourceCoordinates = place.location?.coordinate
                self.getCoordinates(destination: self.destinationAddress)
            } else {
                self.showAlert()
            }
        }
    }
    
    func getCoordinates(destination: String) {
        
        let locator = CLGeocoder()
        
        locator.geocodeAddressString(destination)
        { (placemarks, errors) in
            
            if let place = placemarks?[0] {
                
                self.destinationCoordinates = place.location?.coordinate
                self.drawRouteOnMap(sourceLocation: self.sourceCoordinates!, destinationLocation: self.destinationCoordinates!)
            } else {
                self.showAlert()
            }
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        loadMap()
        
        sourceTextField.returnKeyType = .done
        destinationTextField.returnKeyType = .done
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        // dismissing the keyboard
        self.view.endEditing(true)
        return true
    }
    
    func loadMap(){
        
        locationManager = CLLocationManager()
        locationManager.requestWhenInUseAuthorization()
        
        mapView!.mapType = MKMapType.standard
        mapView!.showsUserLocation = true
        mapView!.showsTraffic = true
        mapView!.region = getSanDiegoLocation()
    }

    func getSanDiegoLocation() -> MKCoordinateRegion {
        
        let sanDiegoRegion = CLLocationCoordinate2DMake(32.7157, -117.1611)
        let latitudinalMeters:CLLocationDistance = 100
        let longitudinalMeters:CLLocationDistance = 1000*120
        
        return MKCoordinateRegion(center: sanDiegoRegion, latitudinalMeters: latitudinalMeters, longitudinalMeters: longitudinalMeters)
    }
    
    func drawRouteOnMap(sourceLocation: CLLocationCoordinate2D, destinationLocation: CLLocationCoordinate2D){
        
        let sourceAnnotation = AnnotatedLocation(coordinate: sourceLocation, title: "A", subtitle: "")
        let destinationAnnotation = AnnotatedLocation(coordinate: destinationLocation, title: "B", subtitle: "")
        
        self.mapView.addAnnotation(sourceAnnotation)
        self.mapView.addAnnotation(destinationAnnotation)
        
        let sourceMark = MKPlacemark(coordinate: sourceLocation)
        let destinationMark = MKPlacemark(coordinate: destinationLocation)
        
        let requestDirection = MKDirections.Request()
        
        requestDirection.source = MKMapItem(placemark: sourceMark)
        requestDirection.destination = MKMapItem(placemark: destinationMark)
        requestDirection.transportType = .automobile
        
        let directions = MKDirections(request: requestDirection)
        
        calculateDirections(directions)
        
        self.mapView.delegate = self
    }
    
    func calculateDirections(_ directions: MKDirections) {
        
        directions.calculate { (response, error) in
            guard let directionResonse = response else {
                if let error = error {
                    print("Error: \(error.localizedDescription)")
                    self.showAlert()
                }
                return
            }
            
            let route = directionResonse.routes[0]
            self.mapView.addOverlay(route.polyline, level: .aboveRoads)
            
            let rectangle = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegion(rectangle), animated: true)
        }
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        let renderer = MKPolylineRenderer(overlay: overlay)
        
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 3.5
        
        return renderer
    }
    
    @IBAction func showAlert() {
        
        let message: String = NSLocalizedString("invalid_address_message", comment: "")
        let alertController = UIAlertController(title: "Error",
                                                message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        
        alertController.addAction(ok)
        
        self.present(alertController, animated: true, completion: {print("Done")})
    }
}
