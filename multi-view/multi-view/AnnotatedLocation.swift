//
//  AnnotatedLocation.swift
//  multi-view
//
//  Created by Darshan Mange on 10/28/18.
//  Copyright © 2018 Darshan Mange. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

class AnnotatedLocation:NSObject, MKAnnotation {
    let coordinate: CLLocationCoordinate2D
    let title:String?
    let subtitle:String?
    
    init(coordinate: CLLocationCoordinate2D,title:String,subtitle:String) {
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
    }
}
