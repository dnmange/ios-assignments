//
//  StateController.swift
//  multi-view
//
//  Created by Darshan Mange on 10/27/18.
//  Copyright © 2018 Darshan Mange. All rights reserved.
//

import UIKit

class StateController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var stateNames:[String] = []
    var states:[String : [String: String]] = [:]
    var selectedName:String = ""
        
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stateNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // this method is called on each item in the states and returns tablecell for respective item
        
        let tableCell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cellIdentifier")
        tableCell.textLabel?.text = stateNames[indexPath.row]
        return tableCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let row = indexPath.row
        selectedName = stateNames[row]
        self.performSegue(withIdentifier: "DetailedState", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // pass selected state details to Detailed Scene
        if segue.identifier == "DetailedState" {
            
            guard let controller = segue.destination as? DetailedStateController else {
                return
            }
            controller.stateName = selectedName
            controller.stateDescription = states[selectedName] as! [String: String]
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        loadDataFromPList()
    }
    
    @IBAction func back(unwindSegue:UIStoryboardSegue) {
       
    }

    func loadDataFromPList(){
        
        guard let path = Bundle.main.path(forResource: "states", ofType: "plist"),
            let data = NSDictionary(contentsOfFile: path) else {
                return
        }
        
        for (key,value) in data{
            if let name = key as? String{
                stateNames.append(name)
                if let values = value as? [String:String]{
                    states[name] = values
                }
            }
        }
    }
}
