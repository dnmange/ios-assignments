//
//  DetailedStateController.swift
//  multi-view
//
//  Created by Darshan Mange on 10/27/18.
//  Copyright © 2018 Darshan Mange. All rights reserved.
//

import UIKit

class DetailedStateController: UIViewController {
    
    var stateName:String = ""
    var stateDescription:[String: String] = [:]         
    
    @IBOutlet weak var stateNameLabel: UILabel!
    @IBOutlet weak var abbreviationLabel: UILabel!
    
    @IBOutlet weak var sinceLabel: UILabel!
    @IBOutlet weak var capitalLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLabels()
    }
    
    func setLabels(){
        
        stateNameLabel.text = stateName
        abbreviationLabel.text = stateDescription["abbreviation"]
        capitalLabel.text = stateDescription["capital"]
        sinceLabel.text = stateDescription["since"]
    }

}
