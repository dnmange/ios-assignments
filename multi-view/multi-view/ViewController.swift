//
//  ViewController.swift
//  multi-view
//
//  Created by Darshan Mange on 10/27/18.
//  Copyright © 2018 Darshan Mange. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    
    @IBOutlet weak var nameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    
    @IBOutlet weak var statesButton: UIButton!
    @IBOutlet weak var tabButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setLabels();
        
        nameText.returnKeyType = .done
        passwordText.returnKeyType = .done
        
        nameText.delegate = self
        passwordText.delegate = self
    }

    func setLabels() {
        
        nameLabel.text = NSLocalizedString("name", comment: "")
        passwordLabel.text = NSLocalizedString("password", comment: "")
        tabButton.setTitle(NSLocalizedString("tab", comment: ""), for: .normal)
        statesButton.setTitle(NSLocalizedString("state", comment: ""), for: .normal)
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return isValid()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? TabController {
            destination.name = nameText.text!
        }
    }
    
    func isValid() -> Bool{
        guard let name = nameText.text, let password = passwordText.text else {
            showAlert()
            return false;
        }
        
        if name.count < 4 || password.count < 4 {
            showAlert()
            return false
        }
        
        return true
    }
    
    @IBAction func back(unwindSegue:UIStoryboardSegue) {
        if let source = unwindSegue.source as? NameTabController {
            nameText.text = source.nameTextField.text
        }
    }
    
    @IBAction func showAlert() {
        
        let message: String = NSLocalizedString("invalid_length_message", comment: "")
        let alertController = UIAlertController(title: "Error",
                                                message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(ok)
        self.present(alertController, animated: true, completion: {print("Done")})
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        // dismissing the keyboard
        self.view.endEditing(true)
        return true
    }
}

