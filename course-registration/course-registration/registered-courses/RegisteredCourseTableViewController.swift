//
//  RegisteredCourseTableViewController.swift
//  course-registration
//
//  Created by Darshan Mange on 11/17/18.
//  Copyright © 2018 Darshan Mange. All rights reserved.
//

import UIKit
import Alamofire

class RegisteredCourseTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var coursesTabController: CoursesTabBarController?
    var courseDetailedList: [CourseDescription] = []
    var selectedIndex: Int = -1
    
    @IBOutlet weak var registeredCourseTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        coursesTabController = tabBarController as! CoursesTabBarController
       
        // reload enrolled courses when add/ drop occurs
        NotificationCenter.default.addObserver(self, selector:  #selector(loadRegisteredCourseInfo), name: NSNotification.Name("updateEnrollWaitlistCourses"), object: nil)
        loadRegisteredCourseInfo()
        
        self.setCourseTable()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? CourseDetailViewController {
            dest.courseInfo = courseDetailedList[self.selectedIndex]
            dest.courseAction = CourseAction.removeCourse
        }
    }
    
    @objc func loadRegisteredCourseInfo(){
        let parameters = [
            "classids": coursesTabController?.classInfo["classes"],
        ]
        
        let url = NSLocalizedString("CLASS_DETAILS_URL", comment: "")
        
        Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let JSON = response.result.value {
                    
                    if let jsonObject = JSON as? NSDictionary {
                        if let val = jsonObject["error"] {
                            
                            self.coursesTabController?.errorMessage = val as? String
                            self.coursesTabController?.showAlert()
                            return
                        }
                        return
                    }
                    self.courseDetailedList = []
                    if let jsonArr = JSON as? NSArray {
                        for val in jsonArr {
                            self.courseDetailedList.append(CourseDescription(jsonObject: val as! NSDictionary))
                        }
                    }
                    
                    self.registeredCourseTable.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func setCourseTable(){
        if registeredCourseTable != nil {
            registeredCourseTable!.delegate = self
            registeredCourseTable!.dataSource = self
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courseDetailedList.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let courseDescription: CourseDescription = courseDetailedList[indexPath.row]
        let course: String = courseDescription.course ?? ""
        
        let title: String = courseDescription.courseTitle ?? ""
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cellIdentifierRegisteredCourse")
        
        cell.textLabel?.text = course + " " + title
        
        return cell
    }
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let row = indexPath.row
        self.selectedIndex = indexPath.row
        self.performSegue(withIdentifier: "addedCourseInfo", sender: self)
        
    }

    @IBAction func backToRegistered(unwindSegue:UIStoryboardSegue) {
      
    }
    
    @IBAction func logOut(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "shouldLogin")
        ChangeStoryBoard.updateRoot()
    }

}
