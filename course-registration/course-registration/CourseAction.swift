//
//  CourseAction.swift
//  course-registration
//
//  Created by Darshan Mange on 11/21/18.
//  Copyright © 2018 Darshan Mange. All rights reserved.
//

import Foundation

enum CourseAction{
    case addCourseWaitlist
    case removeCourseWaitlist
    case addCourse
    case removeCourse
}
