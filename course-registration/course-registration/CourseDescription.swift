//
//  CourseDescription.swift
//  course-registration
//
//  Created by Darshan Mange on 11/19/18.
//  Copyright © 2018 Darshan Mange. All rights reserved.
//

import Foundation

class CourseDescription {
    
    var course: String?
    var courseId: Int?
    var courseTitle: String?
    var section: String?
    var scheduleNo: String?
    
    var units: String?
    var seats: String?
    
    var meetings: String?
    var fullTitle: String?
    var description: String?
    
    var prerequiste: String?
    
    init(jsonObject: NSDictionary){
        
        courseTitle = jsonObject["title"] as? String
        
        let subject: String = jsonObject["subject"] as? String ?? "NA"
        let coourseNo: String = jsonObject["course#"] as? String ?? "NA"
        
        course = subject + "-" + coourseNo
        section = jsonObject["section"] as? String
        scheduleNo = jsonObject["schedule#"] as? String
        
        units = jsonObject["units"] as? String
        
        let enrolled: String = String(jsonObject["enrolled"] as? Int ?? 0)
        let totalSeats: String = String (jsonObject["seats"] as? Int ?? 0)
        
        seats = enrolled + "/" + totalSeats
        
        let meetingType: String = jsonObject["meetingType"] as? String ?? ""
        
        let startTime: String = jsonObject["startTime"] as? String ?? ""
        let endTime: String = jsonObject["endTime"] as? String ?? ""
        let time: String = startTime + "-" + endTime
        
        let days: String = jsonObject["days"] as? String ?? ""
        let room: String = jsonObject["room"] as? String ?? ""
        let prof: String = jsonObject["instructor"] as? String ?? ""
        
        let locationInfo: String = days + " " + room + " " + prof
        meetings = meetingType + " " + time + " " + locationInfo
        
        fullTitle = jsonObject["fullTitle"] as? String
        description = jsonObject["description"] as? String
        prerequiste = jsonObject["prerequisite"] as? String
        
        courseId = jsonObject["id"] as? Int
    }
}
