//
//  LogInViewController.swift
//  course-registration
//
//  Created by Darshan Mange on 11/17/18.
//  Copyright © 2018 Darshan Mange. All rights reserved.
//

import UIKit
import Alamofire

class LogInViewController: UIViewController {

    @IBOutlet weak var redIdText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    
    let userDefaults = UserDefaults.standard
    var classesInfo: [String: [Int]] = [:]
    var errorMessage: String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // dismiss keyboard
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? CoursesTabBarController {
            
            destination.classInfo = self.classesInfo
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        return false;
    }
    
    override func viewWillAppear(_ animated: Bool) {
        (redIdText.text, passwordText.text) = retrieveDataFromDefaults();
    }
    
    @IBAction func logIn(_ sender: Any) {
        
        if !isValid() {
            return
        }
        
        let parameters = [
            "redid": redIdText.text,
            "password": passwordText.text
        ]
        
        let url = NSLocalizedString("STUDENT_CLASSES", comment: "")
        
        Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let JSON = response.result.value {
                    
                    let jsonObject = JSON as! NSDictionary
                    
                    if let val = jsonObject["error"] {
                        
                        self.errorMessage = val as! String
                        self.showAlert()
                        return
                    }
                    
                    self.classesInfo = jsonObject as! [String : [Int]]
                    self.setUserDetailsToDefaults(self.redIdText.text!, self.passwordText.text!)
                    
                    self.userDefaults.set(true, forKey: "shouldLogin")
                    ChangeStoryBoard.updateRoot()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @IBAction func showAlert() {
        let message: String = self.errorMessage
        let alertController = UIAlertController(title: NSLocalizedString("error", comment: ""),
                                                message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: nil)
        alertController.addAction(ok)
        self.present(alertController, animated: true, completion: {print("Done")})
    }
    
    
    func setUserDetailsToDefaults(_ redId: String, _ password: String){
        
        userDefaults.set(redId, forKey: "redid")
        userDefaults.set(password, forKey: "password")
    }
    
    func isValid() -> Bool{
        guard let redId = redIdText.text, let password = passwordText.text else {
            return false;
        }
        
        return true
    }
    
    func retrieveDataFromDefaults() -> (String?,String?) {
        return (userDefaults.string(forKey: "redid"), userDefaults.string(forKey: "password"))
    }
    
}
