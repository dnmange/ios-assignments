//
//  CoursesTabBarController.swift
//  course-registration
//
//  Created by Darshan Mange on 11/17/18.
//  Copyright © 2018 Darshan Mange. All rights reserved.
//

import UIKit
import Alamofire

class CoursesTabBarController: UITabBarController {

    var classInfo: [String: [Int]] = [:]
    var errorMessage: String?

    let userDefaults: UserDefaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // this is triggered when add/drop course occur
        NotificationCenter.default.addObserver(self, selector:  #selector(reloadClassIds), name: NSNotification.Name("reloadClassIds"), object: nil)
        
    }

    @IBAction func showAlert() {
        let message: String = self.errorMessage ?? ""
        let alertController = UIAlertController(title: NSLocalizedString("error", comment: ""),
                                                message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: nil)
        alertController.addAction(ok)
        self.present(alertController, animated: true, completion: {print("Done")})
    }
    
    override func viewDidAppear(_ animated: Bool) {
        reloadClassIds()
    }
    
    @objc func reloadClassIds(){
        let parameters = [
            "redid": userDefaults.string(forKey: "redid"),
            "password": userDefaults.string(forKey: "password")
            ] as [String : Any]
        
        let url = NSLocalizedString("STUDENT_CLASSES", comment: "")
        
        Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let JSON = response.result.value {
                    
                    let jsonObject = JSON as! NSDictionary
                    
                    if let val = jsonObject["error"] {
                        
                        self.errorMessage = val as! String
                        self.showAlert()
                        return
                    }
                    
                    self.classInfo = jsonObject as! [String : [Int]]
                    
                    // trigger enrolled and waitlist data
                    NotificationCenter.default.post(name: NSNotification.Name("updateEnrollWaitlistCourses"), object: self)
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
}
