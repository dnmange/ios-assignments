//
//  RegisterViewController.swift
//  course-registration
//
//  Created by Darshan Mange on 11/17/18.
//  Copyright © 2018 Darshan Mange. All rights reserved.
//

import UIKit
import Alamofire

class RegisterViewController: UIViewController {

    
    @IBOutlet weak var fnameText: UITextField!
    @IBOutlet weak var lnameText: UITextField!
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var redIdText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    
    let userDefaults: UserDefaults = UserDefaults.standard
    
    var classesInfo: [String: [Int]] = [:]
    var errorMessage: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        (redIdText.text, passwordText.text, fnameText.text, lnameText.text, emailText.text) = retrieveDataFromDefaults();
    
        // dismiss keyboard
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
        
    }
    
    @IBAction func registerStudent(_ sender: Any) {
        if !isValid() {
            return
        }
        
        let parameters = [
            "firstname": fnameText.text,
            "lastname": lnameText.text,
            "redid": redIdText.text,
            "password": passwordText.text,
            "email": emailText.text
        ]
        
        let url = NSLocalizedString("ADD_STUDENT_URL", comment: "")
        
        Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let JSON = response.result.value {
                    
                    let jsonObject = JSON as! NSDictionary
                    
                    if let val = jsonObject["error"] {
                        self.errorMessage = val as! String
                        self.showAlert()
                        return
                    }
                    
                    // save user info
                    self.setUserDetailsToDefaults()
                    self.goToLogin()
                    
                }
            case .failure(let error):
                print(error)
            }
        }

    }
    
    @IBAction func showAlert() {
        
        var message: String = self.errorMessage
        let alertController = UIAlertController(title: NSLocalizedString("error", comment: ""),
                                                message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: nil)
        alertController.addAction(ok)
        self.present(alertController, animated: true, completion: {print("Done")})
    }
    
    func goToLogin() {
        tabBarController?.selectedIndex = 0;
    }
    
    func setUserDetailsToDefaults(){
        
        userDefaults.set(self.redIdText.text!, forKey: "redid")
        userDefaults.set(self.passwordText.text!, forKey: "password")
        
        userDefaults.set(self.fnameText.text!, forKey: "firstName")
        userDefaults.set(self.lnameText.text!, forKey: "lastName")
        userDefaults.set(self.emailText.text!, forKey: "email")
    }
    
    func isValid() -> Bool{
        guard let redId = redIdText.text, let password = passwordText.text,
            let fname = fnameText.text, let lname = lnameText.text, let email = emailText.text else {
            return false
        }
        
        if redId.count < 4 && password.count < 8{
            return false
        }
        
        return true
    }
    
    func retrieveDataFromDefaults() -> (String?, String?, String?, String?, String?) {
        return (userDefaults.string(forKey: "redid"), userDefaults.string(forKey: "password"), userDefaults.string(forKey: "firstName"), userDefaults.string(forKey: "lastName"),
            userDefaults.string(forKey: "email"))
    }
    
}
