//
//  CourseDetailViewController.swift
//  course-registration
//
//  Created by Darshan Mange on 11/17/18.
//  Copyright © 2018 Darshan Mange. All rights reserved.
//

import UIKit
import Alamofire

class CourseDetailViewController: UIViewController {

    var courseInfo: CourseDescription?
    var courseAction: CourseAction?
    
    var message: String?
    var actionStatus: Status?
    
    enum Status {
        case success
        case error
    }
    
    var userDefaults: UserDefaults = UserDefaults.standard
    
    @IBOutlet weak var tilteLabel: UILabel!
    
    @IBOutlet weak var courseLabel: UILabel!
    
    @IBOutlet weak var sectionLabel: UILabel!
    
    @IBOutlet weak var meetingsLabel: UILabel!
    
    @IBOutlet weak var seatsLabel: UILabel!
    @IBOutlet weak var unitsLabel: UILabel!
    @IBOutlet weak var scheduleLabel: UILabel!
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var courseActionBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setLabelValues()
    }
    
    @IBAction func addDrop(_ sender: Any) {
    
        // functionality of view controller changes depending on parent
        if courseAction! == CourseAction.addCourse {
            handleAddCourse(NSLocalizedString("ADD_ENROLL_URL", comment: ""))
        } else if courseAction! == CourseAction.removeCourse{
            handleDropCourse(NSLocalizedString("DROP_ENROLL_URL", comment: ""))
        } else if courseAction! == CourseAction.removeCourseWaitlist{
            handleDropCourse(NSLocalizedString("DROP_WAIT_LIST_URL", comment: ""))
        }
    }
    
    // recursively call for enroll as well as waitlist course
    func handleAddCourse(_ url: String) {
        
        let parameters = [
            "redid": userDefaults.string(forKey: "redid"),
            "password": userDefaults.string(forKey: "password"),
            "courseid": self.courseInfo!.courseId!
            ] as [String : Any]
        
        //self.sender = sender
        Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let JSON = response.result.value {
                    
                    let jsonObject = JSON as! NSDictionary
                    
                    if let val = jsonObject["error"] {
                        
                        self.actionStatus = Status.error
                        
                        // check if student can be added in waitlist
                        if("\(val)" == NSLocalizedString("courseFull", comment: "")) {
                            self.message = ""
                            self.courseAction = CourseAction.addCourseWaitlist
                            self.title = NSLocalizedString("waitlistMessage", comment: "")
                            self.showAlert()
                            return
                        }
                        self.courseAction = nil
                        self.message = val as! String
                        self.title = NSLocalizedString("error", comment: "")
                        self.showAlert()
                        return
                    }
                    
                    if let val = jsonObject["ok"] {
                        self.actionStatus = Status.success
                        
                        self.message = val as! String
                        self.title = NSLocalizedString("success", comment: "")
                        self.actionStatus = Status.success
                        self.showAlert()
                        
                        // reloads classids as course is added
                        self.triggerClassIds()
                        return
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func triggerClassIds() {
        NotificationCenter.default.post(name: NSNotification.Name("reloadClassIds"), object: self)
    }
    
    func handleDropCourse(_ url: String) {
        let parameters = [
            "redid": userDefaults.string(forKey: "redid"),
            "password": userDefaults.string(forKey: "password"),
            "courseid": self.courseInfo!.courseId!
            ] as [String : Any]
        
        Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let JSON = response.result.value {
                    
                    let jsonObject = JSON as! NSDictionary
                    
                    if let val = jsonObject["error"] {
                        
                        self.actionStatus = Status.error
                        self.message = val as! String
                        self.title = NSLocalizedString("error", comment: "")
                        self.courseAction = nil
                        self.showAlert()
                        return
                    }
                    
                    if let val = jsonObject["ok"] {
                        self.actionStatus = Status.success
                        
                        self.message = val as! String
                        self.title = NSLocalizedString("success", comment: "")
                        self.showAlert()
                        
                        // reloads classids as course is droped
                        self.triggerClassIds()
                        return
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func setLabelValues(){
        
        if let courseObj = courseInfo {
            
            courseLabel.text = courseObj.course
            tilteLabel.text = courseObj.courseTitle
            sectionLabel.text = courseObj.section
            scheduleLabel.text = courseObj.scheduleNo
            unitsLabel.text = courseObj.units
            seatsLabel.text = courseObj.seats
            meetingsLabel.text = courseObj.meetings
            descriptionText.text = courseObj.description
        }
        
        switch courseAction! {
        case .addCourse, .addCourseWaitlist:
            courseActionBtn!.setTitle(NSLocalizedString("add", comment: ""), for: .normal)
        case .removeCourse, .removeCourseWaitlist:
            courseActionBtn!.setTitle(NSLocalizedString("drop", comment: ""), for: .normal)
        }
    }

    func backToParent(){
        
        if let action = courseAction {
            switch courseAction! {
                
            case .addCourseWaitlist, .addCourse:
                self.performSegue(withIdentifier: "backToFilterView", sender: self)
            case .removeCourseWaitlist:
                self.performSegue(withIdentifier: "backToWaitlist", sender: self)
            case .removeCourse:
                self.performSegue(withIdentifier: "backToRegistered", sender: self)
            }
            return
        }
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func showAlert() {
        let message: String = self.message!
        
        // alert is changed for waitlist action
        if courseAction == CourseAction.addCourseWaitlist && actionStatus == Status.error{
            let alertController = UIAlertController(title: title!,
                                                    message: message, preferredStyle: .alert)
            let okAction = UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default){
                action in NSLog("Navigation")
                let url = NSLocalizedString("ADD_WAIT_LIST_URL", comment: "")
                self.handleAddCourse(url)
 
            }
            let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .cancel, handler: nil)
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: {})
        } else {
            let alertController = UIAlertController(title: title!,
                                                    message: message, preferredStyle: .alert)
            let ok = UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default){
                action in NSLog("Navigation")
                self.backToParent()
            }
            alertController.addAction(ok)
            self.present(alertController, animated: true, completion: {print("Done")})
        }
    }

}
