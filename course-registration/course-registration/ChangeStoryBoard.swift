//
//  ChangeStoryBoard.swift
//  course-registration
//
//  Created by Darshan Mange on 11/21/18.
//  Copyright © 2018 Darshan Mange. All rights reserved.
//

import Foundation
import UIKit

class ChangeStoryBoard {
    
    // change root depending on login status
    static func updateRoot() {
        
        let shouldLogin = UserDefaults.standard.bool(forKey: "shouldLogin")
        
        var rootViewController : UIViewController?
        
        if shouldLogin == true {
            rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "coursesTabBar") as! CoursesTabBarController
        }else{
            rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginTabBar") as! MainTabBarController
        }
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = rootViewController
    }
}
