//
//  MajorViewController.swift
//  course-registration
//
//  Created by Darshan Mange on 11/17/18.
//  Copyright © 2018 Darshan Mange. All rights reserved.
//

import UIKit
import Alamofire

class MajorViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var majorTable: UITableView!
    
    var majorsInfo: [String: Int] = [:]
    var majorList: [String] = []
    var errorMessage: String = ""
    
    var selectedMajor:String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadTableData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return majorList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cellIdentifierMajor")
        
        cell.textLabel?.text = majorList[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let row = indexPath.row
        self.selectedMajor = majorList[indexPath.row]
        self.performSegue(withIdentifier: "backToFilterView", sender: self)
        
    }
    
    func loadTableData() {
        loadMajors()
        if majorTable != nil {
            majorTable!.delegate = self
            majorTable!.dataSource = self
        }
    }
    
    
    func loadMajors(){
        let parameters: [String : Any] = [:]
        
        let url = NSLocalizedString("SUBJECT_LIST_URL", comment: "")
        Alamofire.request(url,
                          method: .get,
                          parameters: parameters)
            .responseJSON { response in
                switch response.result {
                case .success:
                    if let JSON = response.result.value {
                        
                        if let jsonObject = JSON as? NSDictionary {
                            if let val = jsonObject["errors"] {
                                
                                self.errorMessage = val as! String
                                self.showAlert()
                            }
                            return
                        }
                        
                        let jsonArr = JSON as! NSArray
                        
                        for val in jsonArr {
                            var major = val as! [String: Any]
                            
                            self.majorList.append(major["title"] as! String)
                            self.majorsInfo[major["title"] as! String] = major["id"] as! NSInteger
                        }
                        
                        self.majorTable.reloadData()
                    }
                case .failure(let error):
                    print(error)
                }
        }
    }
    
    @IBAction func showAlert() {
        var message: String = self.errorMessage
        let alertController = UIAlertController(title: NSLocalizedString("error", comment: ""),
                                                message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: nil)
        alertController.addAction(ok)
        self.present(alertController, animated: true, completion: {print("Done")})
    }
    
    @IBAction func logOut(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "shouldLogin")
        ChangeStoryBoard.updateRoot()
    }

}
