//
//  FilterViewController.swift
//  course-registration
//
//  Created by Darshan Mange on 11/17/18.
//  Copyright © 2018 Darshan Mange. All rights reserved.
//

import UIKit
import Alamofire

class FilterViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var levelPicker: UIPickerView!
    
    @IBOutlet weak var startTimeText: UITextField!
    @IBOutlet weak var endTimeText: UITextField!
    
    @IBOutlet weak var majorLabel: UILabel!
    
    let timePattern: String = "([01]?[0-9]|2[0-3])[0-5][0-9]"
    var errorMessage: String = ""
    
    var level: [String]?
    var selectedLevelIndex: Int!
    var selectedSubjectId: Int?
    
    var classIds: [Int]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadLevelPicker()
        
        // dismiss keyboard
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:)))
        tap.cancelsTouchesInView = false
        self.view.addGestureRecognizer(tap)
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier.elementsEqual("searchCourseList"){
            return false;
        }
        return true;
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // if navigation is attached
        if let dest = segue.destination as? UINavigationController{
            if let controller = dest.viewControllers.first as? CoursesListTableViewController{
                controller.classIds = self.classIds
            }
        }
        
        if let dest = segue.destination as? CoursesListTableViewController{
            dest.classIds = self.classIds
        }
    }
    
    func isTimeValid(_ text:String) -> Bool{
        return text.matches(self.timePattern)
    }
    
    @IBAction func logOut(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "shouldLogin")
        ChangeStoryBoard.updateRoot()
    }
    
    func loadLevelPicker(){
        
        let data:Bundle = Bundle.main
        let levelList:String? = data.path(forResource: "LevelList", ofType: "plist")
        
        if levelList != nil {
            level = (NSArray.init(contentsOfFile: levelList!) as! Array)
        }
        
        // setting picker connections to view controller
        if levelPicker != nil {
            levelPicker!.delegate = self
            levelPicker!.dataSource = self
        }
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        guard (level != nil) else { return 0 }
        
        return level!.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int,
                    forComponent component: Int) -> String? {
        
        guard (level != nil) else { return "None" }
        
        return level![row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int,
                    inComponent component: Int) {
        
        guard (level != nil) else { return }
        if component == 0 {
            selectedLevelIndex = row
        }
    }
    
    @IBAction func searchCourses(_ sender: Any) {
        loadClassIds()
    }
    
    func loadClassIds() {
        var parameters: [String : Any] = [:]
        
        if let index = selectedLevelIndex {
            parameters["level"] = level![index]
        }
        
        if startTimeText!.text != "" {
            
            if !isTimeValid(startTimeText!.text!){
                self.errorMessage = NSLocalizedString("startTimeInvalid", comment: "")
                self.showAlert()
                return
            }
            parameters["starttime"] = startTimeText!.text
        }
        
        if endTimeText!.text != "" {
            if !isTimeValid(endTimeText!.text!){
                self.errorMessage = NSLocalizedString("endTimeInvalid", comment: "")
                self.showAlert()
                return
            }
            parameters["endtime"] = endTimeText!.text
        }
        
        if let id = selectedSubjectId {
            parameters["subjectid"] = id
        } else {
            self.errorMessage = NSLocalizedString("majorInvalid", comment: "")
            showAlert()
            return
        }
        
        let url = NSLocalizedString("CLASS_ID_LIST_URL", comment: "")
        
        Alamofire.request(url,
                          method: .get,
                          parameters: parameters)
            .responseJSON { response in
            switch response.result {
            case .success:
                if let JSON = response.result.value {
                    
                    if let jsonObject = JSON as? NSDictionary {
                        if let val = jsonObject["errors"] {
                            
                            self.errorMessage = val as! String
                            self.showAlert()
                        }
                        return
                    }
                    
                    let jsonArr = JSON as! NSArray
                    
                    self.classIds = (jsonArr as! [Int])
                    self.performSegue(withIdentifier: "searchCourseList", sender: self)
                }
            case .failure(let error):
                print(error)
            }
        }

    }
    
    @IBAction func showAlert() {
        let message: String = self.errorMessage
        let alertController = UIAlertController(title: "Error",
                                                message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: nil)
        alertController.addAction(ok)
        self.present(alertController, animated: true, completion: {print("Done")})
    }
    
    @IBAction func backToFilterView(unwindSegue:UIStoryboardSegue) {
        if let source = unwindSegue.source as? MajorViewController {
            majorLabel.text = source.selectedMajor
            selectedSubjectId = source.majorsInfo[source.selectedMajor!]
        }
    }

}

extension String {
    func matches(_ regex: String) -> Bool {
        return self.range(of: regex, options: .regularExpression, range: nil, locale: nil) != nil
    }
}
