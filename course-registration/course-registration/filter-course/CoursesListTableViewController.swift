//
//  CoursesListTableViewController.swift
//  course-registration
//
//  Created by Darshan Mange on 11/17/18.
//  Copyright © 2018 Darshan Mange. All rights reserved.
//

import UIKit
import Alamofire

class CoursesListTableViewController: UITableViewController  {
    
    var courseDetailedList: [CourseDescription] = []
    var selectedIndex: Int = -1
    var errorMessage: String?

    @IBOutlet var courseInfoTable: UITableView!
    
    var classIds: [Int]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadCourseInfo()
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return courseDetailedList.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let courseDescription: CourseDescription = courseDetailedList[indexPath.row]
        let course: String = courseDescription.course ?? ""
        
        let title: String = courseDescription.courseTitle ?? ""
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.default, reuseIdentifier: "cellIdentifierCourseInfo")
        
        cell.textLabel?.text = course + " " + title
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let row = indexPath.row
        self.selectedIndex = indexPath.row
        
        // call CourseDetailedViewController
        self.performSegue(withIdentifier: "filterCourseInfo", sender: self)
        
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dest = segue.destination as? CourseDetailViewController {
            dest.courseInfo = courseDetailedList[self.selectedIndex]
            dest.courseAction = CourseAction.addCourse
        }
    }
    
    func loadCourseInfo(){
        let parameters = [
            "classids": self.classIds,
            ]
        
        let url = NSLocalizedString("CLASS_DETAILS_URL", comment: "")
        
        Alamofire.request(url, method:.post, parameters:parameters,encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                if let JSON = response.result.value {
                    
                    if let jsonObject = JSON as? NSDictionary {
                        if let val = jsonObject["error"] {
                            
                            self.errorMessage = val as? String
                            self.showAlert()
                            return
                        }
                        return
                    }
                    self.courseDetailedList = []
                    
                    if let jsonArr = JSON as? NSArray {
                        for val in jsonArr {
                            self.courseDetailedList.append(CourseDescription(jsonObject: val as! NSDictionary))
                        }
                    }
                    
                    self.courseInfoTable.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        }
    }
    
    @IBAction func showAlert() {
        let message: String = self.errorMessage ?? ""
        let alertController = UIAlertController(title: NSLocalizedString("error", comment: ""),
                                                message: message, preferredStyle: .alert)
        
        let ok = UIAlertAction(title: NSLocalizedString("ok", comment: ""), style: .default, handler: nil)
        alertController.addAction(ok)
        self.present(alertController, animated: true, completion: {print("Done")})
    }

    
    @IBAction func logOut(_ sender: Any) {
        UserDefaults.standard.set(false, forKey: "shouldLogin")
        ChangeStoryBoard.updateRoot()
    }
}
