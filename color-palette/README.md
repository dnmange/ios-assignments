## Abstract
---

This application allows the user to change the color using rgb(red, green, blue) text fields or by using slider with respect to each rgb fields. It also allows the user to retain values if app is killed or in back ground. 

## Application Screen Shots
---

![Color Palette](app-screenshot/color-palette.jpeg)

