//
//  ViewController.swift
//  Assignment2
//
//  Created by Darshan Mange on 9/16/18.
//  Copyright © 2018 Darshan Mange. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var redText: UITextField!
    @IBOutlet weak var greenText: UITextField!
    @IBOutlet weak var blueText: UITextField!
    @IBOutlet weak var backgroundView: UIView!
    
    @IBOutlet weak var redSliderField: UISlider!
    @IBOutlet weak var greenSliderField: UISlider!
    @IBOutlet weak var blueSliderField: UISlider!
    
    let userDefaults = UserDefaults.standard
    
    var location = CGPoint(x: 0, y: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        (redText.text, greenText.text, blueText.text) = retrieveDataFromDefaults()
        
        redSliderField.value = Float(getUnwrappedColorValue(redText))
        greenSliderField.value = Float(getUnwrappedColorValue(greenText))
        blueSliderField.value = Float(getUnwrappedColorValue(blueText))
        
        setColorFromRGB()
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(saveDataToDefaults), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func setBackgroundOnSubmit(_ sender: Any) {
        dismissKeyboard()
        setColorFromRGB()
    }
    
    @IBAction func redSlider(_ sender: UISlider) {
        redText.text = String(Int(sender.value*100))
        setColorFromRGB()
    }
    
    @IBAction func blueSlider(_ sender: UISlider) {
        blueText.text = String(Int(sender.value*100))
        setColorFromRGB()
    }
    
    @IBAction func greenSlider(_ sender: UISlider) {
        greenText.text = String(Int(sender.value*100))
        setColorFromRGB()
    }
    
    func setColorFromRGB() {
        let red:CGFloat = getUnwrappedColorValue(redText)
        let green:CGFloat = getUnwrappedColorValue(greenText)
        let blue:CGFloat = getUnwrappedColorValue(blueText)
        
        setBackgroundColor(red, green, blue)
        setSliderValues(Float(red), Float(green), Float(blue))
    }
    
    func setSliderValues(_ red: Float, _ green: Float, _ blue: Float) {
        redSliderField.value = red
        greenSliderField.value = green
        blueSliderField.value = blue
    }
    
    func setBackgroundColor(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat) {
        backgroundView.backgroundColor = UIColor(red: red, green: green, blue: blue, alpha: 1)
    }
    
    func getUnwrappedColorValue(_ x: UITextField) -> CGFloat {
        guard let value = Int(x.text!) else {
            return 0;
        }
        return CGFloat(value)/100;
    }
    
    @objc func saveDataToDefaults() {
        userDefaults.set(redText.text, forKey: "red")
        userDefaults.set(blueText.text, forKey: "blue")
        userDefaults.set(greenText.text, forKey: "green")
    }
    
    func retrieveDataFromDefaults() -> (String?,String?,String?) {
        return (userDefaults.string(forKey: "red"), userDefaults.string(forKey: "green"), userDefaults.string(forKey: "blue"))
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch! = touches.first
        backgroundView.center = touch.location(in: self.view)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch: UITouch! = touches.first
        backgroundView.center = touch.location(in: self.view)
    }
}
