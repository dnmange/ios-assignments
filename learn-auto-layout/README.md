## Abstract
---
As iPhone and iPad comes in different screen sizes, it becomes difficult to maintain a consistent view across the iPhones and iPads. This application is an attempt to learn how Xcode works with constraints and auto-layout

## Application Screen Shots
---

[iPhone SE](app-screenshots/iphone-se.png)


[iPhone 6](app-screenshots/iphone-6.png)

[iPhone 8](app-screenshots/iphone-8.png)

[iPhone 8 Plus](app-screenshots/iphone-8+.png)

[iPhone X](app-screenshots/iphone-x.jpeg)

[iPad Pro 9.7](app-screenshots/ipad-9.7.png)